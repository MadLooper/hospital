/**
 * Created by KW on 9/16/2017.
 */
public interface IDoctorShiftListener {
    public void DoctorShiftEnded();
    public void DoctorShiftStarted();

}
