import java.util.List;

/**
 * Created by KW on 9/15/2017.
 */
public interface IRoomPatientListener {

     void patientStateChanged(int room, PatientState state);
}
