
/**
 * Created by KW on 9/15/2017.
 */
public enum PatientState {
    GOOD(3), BAD(1), CRITICAL(0), STABLE(2);

    private int value;

    PatientState(int value){
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
