import java.util.List;

/**
 * Created by KW on 9/16/2017.
 */
public class PatientStateChangedEvent implements IEvent {
    private int room;
    private PatientState state;


    public PatientStateChangedEvent(int room, PatientState state) {
        this.room = room;
        this.state = state;

    }

    @Override
    public void run() {
        List<IRoomPatientListener> list = EventDispatcher.instance.getAllObjectsImplementingInterface(IRoomPatientListener.class);
        for (IRoomPatientListener listener : list) {
            listener.patientStateChanged(room, state);


        }
    }
}