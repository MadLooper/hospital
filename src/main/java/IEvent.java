/**
 * Created by KW on 9/15/2017.
 */
public interface IEvent {
    void run();
}
