import java.util.*;
import java.util.concurrent.Executors;

/**
 * Created by KW on 9/15/2017.
 */
public class Doctor extends AbstractUnit implements IRoomPatientListener, Runnable, IDoctorShiftListener {
    private boolean isFree;
    private List<Room> roomsCovered;
    private List<Room> priorityRooms;
    private String name;
    private int howManyloops;
private PriorityQueue<Room> priorityQueue;


    public Doctor(boolean isOnDuty, List<Room> roomsCovered, String name, int howManyloops) {
        this.isFree = isOnDuty;
        this.name = name;
        this.howManyloops = howManyloops;
        this.roomsCovered = roomsCovered;

        this.priorityRooms = new ArrayList<>();
        Executors.newSingleThreadExecutor().submit(this);

        this.priorityQueue = new PriorityQueue<>(new Comparator<Room>() {
            @Override
            public int compare(Room o1, Room o2) {
                return o1.getStan_pacjenta().getValue() > o2.getStan_pacjenta().getValue() ? 1 : -1;
            }
        }) ;
    }


    @Override
    public void patientStateChanged( int room, PatientState state) {
       System.out.println("Patient state changed: " + state + " - go to room number: " + room);
            for (Room x: roomsCovered) {
                if (x.getRoom_id() == room && x.getStan_pacjenta().getValue()==0) {
                    System.out.println("Dude,patient state has been worse! Critical accident!Added to priority line:" + x.getStan_pacjenta());
                    priorityRooms.add(x);
                    return;
                }
            }
    }


    @Override
    public void run() {
        Thread.currentThread().setName(name);
        DoctorShiftStarted();
        while (howManyloops > 0) {
            while (!priorityRooms.isEmpty()) {
                Iterator<Room> it = priorityRooms.iterator();
                System.out.println("Emergency!! Critical accident! Going there " + name);

                while (it.hasNext()) {
                    Room r = it.next();
                    try {
                        handleRoom(r);
                        if (r.getStan_pacjenta().getValue()!=0) {
                            it.remove();
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            for (Room room : roomsCovered) {
                try {
                    handleRoom(room);

                    if(!priorityRooms.isEmpty()){
                        break;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            howManyloops--;

        }
        DoctorShiftEnded();
    }


    public void handleRoom(Room r) throws InterruptedException {
        Random random = new Random();
        int healthPoints = random.nextInt();
        r.setPunkty_życia(r.getPunkty_życia() + random.nextInt());
        Thread.sleep(5000);

    }

    @Override
    public void DoctorShiftEnded() {
        if (howManyloops==0){
            System.out.println("Go home, bro!");
        }
    }

    @Override
    public void DoctorShiftStarted() {
        isFree = false;
        System.out.println("Welcome back, " + name + "! You've got: " + priorityRooms.size() + " critical accidents and: " + roomsCovered.size()+ " routine cases for today. Have fun!");

    }
}
