import java.util.Arrays;


/**
 * Created by KW on 9/15/2017.
 */
public class Hospital {
    public Hospital() {
        Room room = new Room(2, PatientState.GOOD, 120);
        new Doctor(true, Arrays.asList(

                new Room(1, PatientState.GOOD, 50),
                new Room(2, PatientState.CRITICAL, 90),
                new Room(3, PatientState.BAD, 10),
                new Room(4, PatientState.CRITICAL, 90),
                new Room(5, PatientState.GOOD, 80),
                new Room(6, PatientState.STABLE, 30)), "Alex", 3);
        new Doctor(true, Arrays.asList(new Room(7, PatientState.STABLE, 40),
                new Room(8, PatientState.GOOD, 100),
                new Room(9, PatientState.BAD, 14),
                new Room(10, PatientState.GOOD, 120)), "Tony", 5);


    }
}
