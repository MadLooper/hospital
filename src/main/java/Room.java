import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * Created by KW on 9/15/2017.
 */
public class Room extends AbstractUnit {
    private int room_id;
    private PatientState stan_pacjenta;
    private Timer timer;
    private int punkty_życia;


    public Room(int room_id, PatientState stan_pacjenta, int punkty_życia) {
        super();
        this.room_id = room_id;
        this.stan_pacjenta = stan_pacjenta;
        this.punkty_życia = punkty_życia;


        Timer timer = new Timer(10000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random random = new Random();

                Room.this.punkty_życia = Room.this.punkty_życia - random.nextInt(20);
                if (Room.this.punkty_życia < 10) {
                    Room.this.stan_pacjenta = PatientState.CRITICAL;
                }
                if (10 < Room.this.punkty_życia && Room.this.punkty_życia < 30) {
                    Room.this.stan_pacjenta = PatientState.BAD;
                }
                if (30 < Room.this.punkty_życia && Room.this.punkty_życia < 80) {
                    Room.this.stan_pacjenta = PatientState.STABLE;
                }
                if (Room.this.punkty_życia > 80) {
                    Room.this.stan_pacjenta = PatientState.GOOD;
                }
                EventDispatcher.instance.dispatch(new PatientStateChangedEvent(room_id, stan_pacjenta));
            }
        });
        timer.setRepeats(true);
        timer.start();
    }

    public int getRoom_id() {
        return room_id;
    }

    public PatientState getStan_pacjenta() {
        return stan_pacjenta;
    }

    public int getPunkty_życia() {
        return punkty_życia;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public void setStan_pacjenta(PatientState stan_pacjenta) {
        this.stan_pacjenta = stan_pacjenta;
    }

    public void setPunkty_życia(int punkty_życia) {
        this.punkty_życia = punkty_życia;
    }

    @Override
    public String toString() {
        return "Room{" + "room_id=" + room_id + ", stan_pacjenta=" + stan_pacjenta + ", timer=" + timer + ", punkty_życia=" + punkty_życia + '}';
    }
}
